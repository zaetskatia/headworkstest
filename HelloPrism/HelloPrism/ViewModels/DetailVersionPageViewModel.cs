﻿using Prism.Mvvm;
using Prism.Navigation;
using RepositoryService;

namespace HelloPrism.ViewModels
{
    public class DetailVersionPageViewModel : BindableBase, INavigationAware
    {
        private INavigationService _navigationService;

        private const string DetailVersionPageKey = "DetailVersionPageKey";

        private string _description;
        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        private string _versionName;
        public string VersionName
        {
            get => _versionName;
            set => SetProperty(ref _versionName, value);
        }

        public DetailVersionPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }
       
        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters[DetailVersionPageKey] is AndroidVersionModel androidVersionModel)
            {
                Description = androidVersionModel.Description;
                VersionName = androidVersionModel.VersionName;
            }
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }
    }
}
