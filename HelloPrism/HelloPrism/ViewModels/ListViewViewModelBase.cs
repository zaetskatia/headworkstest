﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using RepositoryService;

namespace HelloPrism.ViewModels
{
    public class ListViewViewModelBase : BindableBase, INavigationAware, IDestructible
    {
        private const string DetailVersionPageKey = "DetailVersionPageKey";
        private const string DetailVersionPageNavigationKey = "DetailVersionPage";

        private readonly INavigationService _navigationService;
        private readonly IAndroidVersionsRepository _versionsInfoService;

        public ListViewViewModelBase(IAndroidVersionsRepository versionsInfoService , INavigationService navigationService)
        {
            _navigationService = navigationService;
            _versionsInfoService = versionsInfoService;
        }

        private ObservableCollection<AndroidVersionModel> _androidVersionList;
        public ObservableCollection<AndroidVersionModel> AndroidVersionList
        {
            get => _androidVersionList;
            set => SetProperty(ref _androidVersionList, value);
        }

        private DelegateCommand<ItemTappedEventArgs> _navigateToDetailCommand;
        public DelegateCommand<ItemTappedEventArgs> NavigateToDetailCommand
        {
            get
            {
                if (_navigateToDetailCommand == null)
                {
                    _navigateToDetailCommand = new DelegateCommand<ItemTappedEventArgs>(async selected =>
                    {
                        var param = new NavigationParameters();
                        if (selected == null || !(selected.Item is AndroidVersionModel androidModel))
                        {
                            param.Add(DetailVersionPageKey,
                                new AndroidVersionModel{Id = 0, Description = "defName", VersionName = "defDescription"});
                            }
                        else
                        {
                            param.Add(DetailVersionPageKey, androidModel);
                        }

                        await _navigationService.NavigateAsync(DetailVersionPageNavigationKey, param);
                    });
                }
                return _navigateToDetailCommand;
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            var dataModel = _versionsInfoService.GetAllData();
            AndroidVersionList = new ObservableCollection<AndroidVersionModel>(dataModel);
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {

        }
       
        public void Destroy()
        {

        }
    }
}
