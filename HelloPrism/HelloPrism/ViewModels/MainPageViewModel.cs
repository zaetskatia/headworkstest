﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RepositoryService;

namespace HelloPrism.ViewModels
{
    public class MainPageViewModel : ListViewViewModelBase
    {
        public MainPageViewModel(IAndroidVersionsRepository versionsInfoService, INavigationService navigationService) 
            : base (versionsInfoService, navigationService)
        {
          
        }
    }

}
