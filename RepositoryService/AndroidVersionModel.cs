﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryService
{
    public class AndroidVersionModel : EntityBase
    {
        public string VersionName { get; set; }
        public string Description { get; set; }
    }
}
