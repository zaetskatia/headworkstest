﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryService
{
    public class AndroidVersionsRepository : IAndroidVersionsRepository
    {
        private readonly ObservableCollection<AndroidVersionModel> _collection = MockAndroidVersinModelList.AndroidVersionModels;

        public AndroidVersionModel GetById(int id)
        {
            return GetAllData().SingleOrDefault(model => model.Id == id);
        }

        public ObservableCollection<AndroidVersionModel> GetAllData()
        {
            return _collection;
        }

        public void Create(AndroidVersionModel entity)
        {
            if (!_collection.Contains(entity))
            {
                _collection.Add(entity);
            }
        }

        public void Delete(AndroidVersionModel entity)
        {
            if (_collection.Contains(entity))
            {
                _collection.Remove(entity);
            }
        }

        public void Update(AndroidVersionModel entity)
        {
            if (_collection.Contains(entity))
            {
                _collection[_collection.IndexOf(entity)] = entity;
            }
        }
    }
}
