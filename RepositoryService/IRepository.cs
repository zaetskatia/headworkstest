﻿using System.Collections.ObjectModel;

namespace RepositoryService
{
    public interface IRepository<T> where T : EntityBase
    {
        T GetById(int id);

        ObservableCollection<T> GetAllData();

        void Create(T entity);

        void Delete(T entity);

        void Update(T entity);
    }
}
