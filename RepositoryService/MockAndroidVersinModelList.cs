﻿using System.Collections.ObjectModel;

namespace RepositoryService
{
    public static class MockAndroidVersinModelList
    {
        public static ObservableCollection<AndroidVersionModel> AndroidVersionModels =
            new ObservableCollection<AndroidVersionModel>()
            {
            new AndroidVersionModel{ Id = 1, Description = "Первая стабильная версия системы.Появился магазин приложений Android Market.", VersionName = "Android 1.0"},
            new AndroidVersionModel{ Id = 2, Description = "Изменения в API. Добавлены кнопки «Show» и «Hide» в меню вызова", VersionName = "Android 2.0 «Eclair»"},
            new AndroidVersionModel{ Id = 3, Description = "Возможность установки сторонних клавиатур.Поддержка виджетов и папок на рабочем столе.", VersionName = "Android 3.0 «Honeycomb»"},
            new AndroidVersionModel{ Id = 4, Description = "Добавлена функция мультиязычного голосового поиска.Добавлена бесплатная возможность пошаговой навигации от Google.", VersionName = "Android 4.0 «Ice Cream Sandwich»"},
            new AndroidVersionModel{ Id = 5, Description = "Обновлённый интерфейс, использование технологии Project Butter для получения более плавного UI. Технология включает тройную буферизацию графического конвейера, чтобы добиться отсутствия скачков в частоте кадров при анимации интерфейса, а также технологию вертикальной синхронизации.", VersionName = "Android 4.1 «Jelly Bean»"},
            new AndroidVersionModel{ Id = 6, Description = "Отображение обложек и кнопок управления на экране блокировки при воспроизведении музыки или показе фильмов через Chromecast.Кнопки навигации и панель уведомлений автоматически скрываются.", VersionName = "Android 4.4 «KitKat»"},
            new AndroidVersionModel{ Id = 7, Description = "Первая стабильная версия системы.Появился магазин приложений Android Market.", VersionName = "Android 5.0 «Lollipop»"},
            new AndroidVersionModel{ Id = 8, Description = "Появился Project Volta, благодаря которому операционная система обращается к процессору не одиночными запросами, а пакетами данных, тем самым экономя заряд, в результате чего Nexus 5 может работать на 1,5 часа дольше.", VersionName = "Android 6.0 «Marshmallow»"},
            new AndroidVersionModel{ Id = 9, Description = "Фоновое переключение задач: все открытые приложения и выполняемые операции можно быстро вывести на основной экран с помощью кнопки «Обзор». Двойное нажатие открывает предыдущую задачу, а удерживание позволяет выбрать нужную среди всех доступных. Подобная функция успешно используется в Windows с помощью комбинации Alt + Tab.", VersionName = "Android 7.0 «Nougat»"},
            new AndroidVersionModel{ Id = 10, Description = "В новой версии приложения смогут менять иконки без обновления всего приложения. Это дает возможность приложениям отображать свой статус с помощью иконки.", VersionName = "Android 8.0 «Oreo»"},

        };
    }
}
